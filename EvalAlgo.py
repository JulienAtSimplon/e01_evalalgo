"""exercices algo
Les boucles
EX 1
Ecrire un algorithme qui demande successivement 20 nombres à l’utilisateur, et qui lui dise ensuite quel était le plus grand parmi ces 20 nombres :
Modifiez ensuite l’algorithme pour que le programme affiche de surcroît en quelle position avait été saisie ce nombre :
C’était le nombre numéro 2
"""

def exercice1():
    i = 1
    MaxVal = 0
    Position = 0
    DicoInput = {}
    while i < 21:
        ValInput = int(input('Entrez un nombre : '))
        if ValInput > MaxVal:
            MaxVal = ValInput
            Position = i
        DicoInput[i] = ValInput
        i = i+1
    print('Le nombre le plus grand est :', MaxVal)
    print('Sa position est :', Position)

"""EX 2
Ecrire un algorithme qui demande un nombre de départ, et qui ensuite écrit la table de multiplication de ce nombre, présentée comme suit (cas où l'utilisateur entre le nombre 7) :
Table de 7 : 7 x 1 = 7 7 x 2 = 14 7 x 3 = 21 … 7 x 10 = 70 """

def exercice2():
    ResultStr = ''
    ValInput = int(input('Entrez un nombre : '))
    for i in range(1, 11):
        ResultStr = ResultStr + str(ValInput) + " x " + str(i) + " = " + str(ValInput * i) + " "
    print('Table de', ValInput, ':', ResultStr)

"""EX 3
Ecrire un algorithme qui demande un nombre de départ, et qui calcule sa factorielle.
NB : la factorielle de 8, notée 8 !, vaut 1 x 2 x 3 x 4 x 5 x 6 x 7 x 8"""

def exercice3():
    ResultStr = 1
    ValInput = int(input('Entrez un nombre : '))
    for i in range(1, ValInput + 1):
        ResultStr = ResultStr * i
    print('la factorielle de ', ValInput, 'est :', ResultStr)

"""EX 4
Écrire un algorithme qui permette de connaître ses chances de gagner au tiercé, quarté, quinté et autres impôts volontaires.
On demande à l’utilisateur le nombre de chevaux partants, et le nombre de chevaux joués. Les deux messages affichés devront être :
Dans l’ordre : une chance sur X de gagner Dans le désordre : une chance sur Y de gagner
X et Y nous sont donnés par la formule suivante, si n est le nombre de chevaux partants et p le nombre de chevaux joués (on rappelle que le signe ! signifie "factorielle") :
X = n ! / (n - p) ! 
Y = n ! / (p ! * (n – p) !)"""
def factorial(var):
    my_factorial = 1
    for i in range(1, var + 1):
        my_factorial = my_factorial * i
    return my_factorial

def exercice4():
    horses = int(input('Nombre de chevaux partants ? : '))
    playing = int(input('Nombre de chevaux joués ? : '))
    X = factorial(horses) / factorial(horses - playing)
    Y = factorial(horses) / (factorial(playing) * factorial(horses - playing))
    print('Dans l’ordre : une chance sur', X, 'de gagner')
    print('Dans le désordre : une chance sur', Y, 'de gagner')

"""Traitements divers
EX 5
Faire un algorithme qui détermine la longueur d’une chaîne de caractères."""

def exercice5():
    MyString = 'test'
    counter = 0
    for i in MyString:
        counter = counter + 1
    print(counter)

"""EX 6
Faire une fonction de concaténation (ajoute à la fin de la première chaîne de caractères le contenu de la deuxième chaîne de caractères.) 
Faire une fonction de Comparaison qui compare deux chaînes de caractères suivant l’ordre lexicographique. 
Faire une fonction qui efface une partie de la chaîne en spécifiant une longueur d’effacement et un indice à partir duquel il faut effacer."""

def input_str():
    first_string = str(input('Tapez une première chaîne de caratères : '))
    second_string = str(input('Tapez une seconde chaîne de caratères : '))
    return first_string, second_string

def exercice6a():
    strings = input_str()
    result_str = strings[0] + strings[1]
    print(result_str)

def exercice6b():
    strings = input_str()
    str_list = [strings[0], strings[1]]
    list.sort(str_list)
    print(str_list[0], 'se situe avant', str_list[1], 'dans l’ordre lexicographique.')

def exercice6c(my_str, erase_len, i_target):
    new_str = ""
    for counter, char in enumerate(my_str):
        if counter >= i_target + erase_len:
            new_str = new_str + char
        elif counter < i_target:
            new_str = new_str + char
    print(new_str)


"""EX 7
Ecrire un algorithme qui affiche la moyenne d’une suite d’entiers"""
def exercice7(my_list):
    my_list_average = 0
    for i in my_list:
        my_list_average = my_list_average + i
    print('La moyenne de cette suite est de :', my_list_average / len(my_list))

"""EX 8
Ecrire une fonction max3 qui retourne le maximum de trois entiers 
Ecrire une fonction min3 qui retourne le minimum de trois entiers 
Ecrire une fonction max2 qui retourne le maximum de deux entiers 
Ecrire une fonction max3 qui retourne le maximum de trois entiers en faisant appel à max2"""
def exercice8():
    print('max3 :', max3(1, 2, 3))
    print('min3 :', min3(1, 2, 3))
    print('max2 :', max2(10, 20))
    print('max3bis :', max3bis(10, 30, 20))

def max3(var1, var2, var3):
    my_list = [var1, var2, var3]
    list.sort(my_list)
    return my_list[2]

def min3(var1, var2, var3):
    my_list = [var1, var2, var3]
    list.sort(my_list)
    return my_list[0]

def max2(var1, var2):
    my_list = [var1, var2]
    list.sort(my_list)
    return my_list[1]

def max3bis(var1, var2, var3):
    imbrication = max2(var1, var2)
    return max2(imbrication, var3)


"""EX 9
Ecrire une action qui fournit les félicitations ou l’ajournement d’un élève suivant sa note en utilisant Si-alors-sinon."""
def exercice9(student, grade):
    if grade >= 10:
        print('Félicitations', student, 'vous êtes admis')
    else:
        print('Navré', student, "vous n'êtes pas admis")


# exercice1()
# exercice2()
# exercice3()
# exercice4()
# exercice5()
# exercice6a()
# exercice6b()
# exercice6c('abcdefghij', 4, 4)
# exercice7([1, 2, 3, 4, 5])
# exercice8()
# exercice9('Julien', 10)